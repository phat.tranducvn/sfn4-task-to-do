- Project List To Do with Symfony 4.1
- Installing these dependencies:
    + Acquia Dev Desktop
    + Composer
- create sites/default/settings.php inside this directory.
- import into Acquia Dev Desktop
- eidt ".env" with these to connect database:
    + DATABASE_URL=mysql://root:@127.0.0.1:33067/sfn4_list_to_do
- Run command to install packages for project :
    + composer install
- Run this command to run server:
    + php bin/console server:run
- Some command you may need:
    // make migration before excuting to database
    + php bin/console make:migration
    // Excute migration to database
    + php bin/console doctrine:migrations:migrate

