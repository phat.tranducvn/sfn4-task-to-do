<?php
/**
 * Created by PhpStorm.
 * User: Ryan
 * Date: 11/3/2018
 * Time: 5:17 PM
 */

namespace App\Controller;


use App\Entity\ThingToDo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListToDoController extends AbstractController
{
    /**
     * @Route ("/",name="homepage")
     */
    function index()
    {
        // Lấy danh sách tất cả các việc cần làm thông qua Doctrine
        $listToDo = $this->getDoctrine()->getRepository(ThingToDo::class)->findAll();
        // Render với danh sách việc cần làm
        return $this->render('index.html.twig',array('listToDo' => $listToDo));
    }

    /**
     * @Route ("/add",name="createTask")
     */
    function createTask(Request $request)
    {
        if($request->request->get('taskName')){
            //Lấy giá trị của taskName từ input
            $taskName = $request->request->get('taskName');
            // Object Manager của Doctrine => Chuyên sử dụng để thêm,cập nhật,xóa
            $entityManager = $this->getDoctrine()->getManager();
            // Tạo mới 1 công việc
            $thingToDo = new ThingToDo();
            $thingToDo->setTaskName($taskName);

            $entityManager->persist($thingToDo); // Lúc này chưa thực hiện câu truy vấn SQL
            $entityManager->flush(); // thực hiện câu truy vấn SQL để lưu vào database
            return $this->redirectToRoute('homepage'); // Trở về trang chủ
        }
        return new Response('Save Failed');
    }

    /**
     * @Route ("/delete/{id}", name="deleteTask")
     */
    function deleteTask(Request $request,$id){
        //Lấy task với id = $id
        $task = $this->getDoctrine()->getRepository(ThingToDo::class)->findOneBy(array('id' => $id));
        //Kiểm tra tồn tại task hay không
        if(!isset($taskName)){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($task);
            $entityManager->flush();
            return $this->redirectToRoute('homepage'); // Trở về trang chủ
        }
        return new Response('Delete Failed');
    }
}