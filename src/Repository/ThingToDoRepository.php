<?php

namespace App\Repository;

use App\Entity\ThingToDo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ThingToDo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThingToDo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThingToDo[]    findAll()
 * @method ThingToDo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThingToDoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ThingToDo::class);
    }

//    /**
//     * @return ThingToDo[] Returns an array of ThingToDo objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ThingToDo
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
